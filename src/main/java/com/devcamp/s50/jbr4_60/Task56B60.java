package com.devcamp.s50.jbr4_60;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task56B60 {

	public static void main(String[] args) {
		SpringApplication.run(Task56B60.class, args);
	}

}
